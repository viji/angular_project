---------------project Setup -------------

run: pip install -r requirements.txt

1. Configure the settings.py

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '[DATABASE NAME]',
        'USER': '[MYSQL USERNAME]',
        'PASSWORD': '[MYSQL PASSWORD]',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}

2. create database in Mysql server
3. Run - $python3 manage.py makemigrations
4. $python3 manage.py migrate
5. $python3 manage.py runserver


--------------Angular installation---------------
  
    1.Node
    2.Js
    3.Node Package manager

Typscript:
  
    $npm install -g typscript
    $tsc -v
  
Angular CLI:
 
    $npm install -g @angular/cli
    $ng -v
    $npm -v
    $node -v

Angula App create:

    $ng new project_name

Generate a component

    ng g c component_name

Remove Node Modules:

    $rm -rf  node_modules

Run Project:
    $ng serve
