import { Component, OnInit,Input } from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-add-edit-task',
  templateUrl: './add-edit-task.component.html',
  styleUrls: ['./add-edit-task.component.css']
})
export class AddEditTaskComponent implements OnInit {

  constructor(private service:UserService) { }

  @Input() 
  task:any;
  taskId:string;
  task_project:string;
  task_emp:string;
  task_desc:string;
  task_work_hrs:string;
  EmployeeList:any=[];

  TaskList:any=[];

  ngOnInit(): void {
    this.loadEmployeeList();
  }
  loadEmployeeList(){
    this.service.getAllEmployeeNames().subscribe((data:any)=>{
      this.EmployeeList=data;

      this.taskId=this.task.taskId;
      this.task_project=this.task.task_project;
      this.task_emp=this.task.task_emp;
      this.task_desc=this.task.task_desc;
      this.task_work_hrs=this.task.task_work_hrs;
    });
  }
  addTask(){
    var val = {taskId:this.taskId,
				task_project:this.task_project,
				task_emp:this.task_emp,
				task_desc:this.task_desc,
				task_work_hrs:this.task_work_hrs
                };

    this.service.addTask(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateTask(){
    var val = {taskId:this.taskId,
        task_project:this.task_project,
        task_emp:this.task_emp,
        task_desc:this.task_desc,
        task_work_hrs:this.task_work_hrs
                };

    this.service.updateTask(val).subscribe(res=>{
    alert(res.toString());
    });
  }
  
}



