import { Component, OnInit } from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-show-task',
  templateUrl: './show-task.component.html',
  styleUrls: ['./show-task.component.css']
})
export class ShowTaskComponent implements OnInit {

  constructor(private service:UserService) { }

  TaskList:any=[];
  TaskListview:any=[];

  ModalTitle:string;
  ActivateAddEditTaskComp:boolean=false;
  ActivateviewTaskComp:boolean=false;
  task:any;

  ngOnInit() {
  this.refreshTaskList();
  
  }

	addClick(){
	    this.task={
	      taskId:0,
	      task_project:"",
	      task_emp:"",
	      task_desc:"",
	      task_work_hrs:""

	    }
	    this.ModalTitle="Add Task";
	    this.ActivateAddEditTaskComp=true;


  	}

  editClick(item){
    console.log(item);
    this.task=item;
    this.ModalTitle="Edit Task";
    this.ActivateAddEditTaskComp=true;
  }

  deleteClick(item){
    if(confirm('Are you sure??')){
      this.service.deleteTask(item.taskId).subscribe(data=>{
        alert(data.toString());
        this.refreshTaskList();
      })
    }
  }

  closeClick(){
    this.ActivateAddEditTaskComp=false;
    this.ActivateviewTaskComp=false;
    this.refreshTaskList();
  }
  viewClick(item){
    this.task=item;
      this.service.viewTask(item.taskId).subscribe(data=>{
        this.TaskListview=data;
        this.ActivateviewTaskComp=true;
        this.task=item;
      })
    }
  

  refreshTaskList(){
    this.service.getTaskList().subscribe(data=>{
      this.TaskList=data;
    });
  }

}
