import { Component, OnInit,Input } from '@angular/core';
import { UserService } from '../../_services';

@Component({
  selector: 'app-add-edit-pro',
  templateUrl: './add-edit-pro.component.html',
  styleUrls: ['./add-edit-pro.component.css']
})
export class AddEditProComponent implements OnInit {

  constructor(private service:UserService) { }

  @Input() 
  pro:any;
  projectId:string;
  project_name:string;
  project_url:string;

  DepartmentsList:any=[];

  ngOnInit(): void {
    this.loadDepartmentList();
  }
  loadDepartmentList(){
    this.service.getAllDepartmentNames().subscribe((data:any)=>{
      this.DepartmentsList=data;

      this.projectId=this.pro.projectId;
      this.project_name=this.pro.project_name;
      this.project_url=this.pro.project_url;
    });
  }
  addProject(){
    var val = {projectId:this.projectId,
                project_name:this.project_name,
                project_url:this.project_url
                };

    this.service.addProject(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateProject(){
    var val = {projectId:this.projectId,
                project_name:this.project_name,
                project_url:this.project_url
                };

    this.service.updateProject(val).subscribe(res=>{
    alert(res.toString());
    });
  }
}
