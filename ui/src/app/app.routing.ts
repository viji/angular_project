﻿import { Routes, RouterModule } from '@angular/router';
import {EmployeeComponent} from './employee/employee.component';
import {ProjectComponent} from './project/project.component';
import {TaskComponent} from './task/task.component';
import { AddEditTaskComponent } from './task/add-edit-task/add-edit-task.component';
import {DepartmentComponent} from './department/department.component';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'employee',component:EmployeeComponent},
	{path:'department',component:DepartmentComponent},
	{ path: 'project',component:ProjectComponent},
	{ path: 'task',component:TaskComponent},
	{ path: 'addedittask/:task',component:AddEditTaskComponent},

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);