﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from '../_models';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
    readonly APIUrl = "http://127.0.0.1:8000";
    readonly PhotoUrl = "http://127.0.0.1:8000/media/";
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }

    register(user: User) {
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }
    adduser(val:any) {
      return this.http.post(this.APIUrl + '/user/',val);
  }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/` + id);
    }
    getDepList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/department/');
  }

  addDepartment(val:any){
    return this.http.post(this.APIUrl + '/department/',val);
  }

  updateDepartment(val:any){
    return this.http.put(this.APIUrl + '/department/',val);
  }

  deleteDepartment(val:any){
    return this.http.delete(this.APIUrl + '/department/'+val);
  }


  getEmpList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/employee/');
  }

  addEmployee(val:any){
    return this.http.post(this.APIUrl + '/employee/',val);
  }

  updateEmployee(val:any){
    return this.http.put(this.APIUrl + '/employee/',val);
  }

  deleteEmployee(val:any){
    return this.http.delete(this.APIUrl + '/employee/'+val);
  }
  viewEmployee(val:any){
    return this.http.get(this.APIUrl + '/employee/'+val);
  }

  UploadPhoto(val:any){
    return this.http.post(this.APIUrl+'/SaveFile',val);
  }

  getAllDepartmentNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/department/');
  }
  addProject(val:any){
    return this.http.post(this.APIUrl + '/project/',val);
  }

  updateProject(val:any){
    return this.http.put(this.APIUrl + '/project/',val);
  }

  deleteProject(val:any){
    return this.http.delete(this.APIUrl + '/project/'+val);
  }
  viewProject(val:any){
    return this.http.get(this.APIUrl + '/project/'+val);
  }
  getProList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/project/');
  }
  addTask(val:any){
    return this.http.post(this.APIUrl + '/task/',val);
  }

  updateTask(val:any){
    return this.http.put(this.APIUrl + '/task/',val);
  }

  deleteTask(val:any){
    return this.http.delete(this.APIUrl + '/task/'+val);
  }
  viewTask(val:any){
    return this.http.get(this.APIUrl + '/task/'+val);
  }
  getTaskList():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl + '/task/');
  }
  getAllEmployeeNames():Observable<any[]>{
    return this.http.get<any[]>(this.APIUrl+'/employee/');
  }
  
}