-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: employeemanagement
-- ------------------------------------------------------
-- Server version	8.0.21-0ubuntu0.20.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add departments',7,'add_departments'),(26,'Can change departments',7,'change_departments'),(27,'Can delete departments',7,'delete_departments'),(28,'Can view departments',7,'view_departments'),(29,'Can add employees',8,'add_employees'),(30,'Can change employees',8,'change_employees'),(31,'Can delete employees',8,'delete_employees'),(32,'Can view employees',8,'view_employees'),(33,'Can add users',9,'add_users'),(34,'Can change users',9,'change_users'),(35,'Can delete users',9,'delete_users'),(36,'Can view users',9,'view_users'),(37,'Can add projects',10,'add_projects'),(38,'Can change projects',10,'change_projects'),(39,'Can delete projects',10,'delete_projects'),(40,'Can view projects',10,'view_projects'),(41,'Can add tasks',11,'add_tasks'),(42,'Can change tasks',11,'change_tasks'),(43,'Can delete tasks',11,'delete_tasks'),(44,'Can view tasks',11,'view_tasks');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$150000$RDlNtXhhotE2$ThcJGTVukRecsbFZJOTI8V1bMNFTO36lcQ0QToWIq9Y=','2020-12-21 06:20:55.588113',1,'employee','','','',1,1,'2020-12-21 06:20:29.917058'),(2,'pbkdf2_sha256$150000$qYN6l9GnDixm$YdAVrBOv2oDTW8KFMknTuP0rhRg3ruYEm2D6QMcLFco=',NULL,0,'admin','','','',0,1,'2020-12-22 07:27:22.677289');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2020-12-21 06:21:13.923652','1','Users object (1)',1,'[{\"added\": {}}]',9,1),(2,'2020-12-21 06:21:30.256502','2','Users object (2)',1,'[{\"added\": {}}]',9,1),(3,'2020-12-21 06:21:48.966290','3','Users object (3)',1,'[{\"added\": {}}]',9,1),(4,'2020-12-21 06:22:21.535248','1','Employees object (1)',1,'[{\"added\": {}}]',8,1),(5,'2020-12-21 06:22:45.374844','2','Employees object (2)',1,'[{\"added\": {}}]',8,1),(6,'2020-12-21 06:23:04.366427','1','Departments object (1)',1,'[{\"added\": {}}]',7,1),(7,'2020-12-21 06:23:08.563871','2','Departments object (2)',1,'[{\"added\": {}}]',7,1),(8,'2020-12-21 06:23:13.736632','3','Departments object (3)',1,'[{\"added\": {}}]',7,1),(9,'2020-12-21 06:23:20.804621','4','Departments object (4)',1,'[{\"added\": {}}]',7,1),(10,'2020-12-21 06:23:26.668874','5','Departments object (5)',1,'[{\"added\": {}}]',7,1),(11,'2020-12-21 06:23:33.461457','6','Departments object (6)',1,'[{\"added\": {}}]',7,1),(12,'2020-12-21 06:23:38.141051','7','Departments object (7)',1,'[{\"added\": {}}]',7,1),(13,'2020-12-21 06:23:44.396235','8','Departments object (8)',1,'[{\"added\": {}}]',7,1),(14,'2020-12-22 07:27:23.179776','2','admin',1,'[{\"added\": {}}]',4,1),(15,'2020-12-30 09:18:32.625014','1','Projects object (1)',1,'[{\"added\": {}}]',10,1),(16,'2020-12-30 09:18:44.571443','2','Projects object (2)',1,'[{\"added\": {}}]',10,1),(17,'2020-12-30 09:19:42.802566','1','Tasks object (1)',1,'[{\"added\": {}}]',11,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'EmployeeApp','departments'),(8,'EmployeeApp','employees'),(10,'EmployeeApp','projects'),(11,'EmployeeApp','tasks'),(9,'EmployeeApp','users'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'EmployeeApp','0001_initial','2020-12-21 06:19:24.823172'),(2,'contenttypes','0001_initial','2020-12-21 06:19:25.823479'),(3,'auth','0001_initial','2020-12-21 06:19:29.621739'),(4,'admin','0001_initial','2020-12-21 06:19:44.014221'),(5,'admin','0002_logentry_remove_auto_add','2020-12-21 06:19:47.757469'),(6,'admin','0003_logentry_add_action_flag_choices','2020-12-21 06:19:47.893641'),(7,'contenttypes','0002_remove_content_type_name','2020-12-21 06:19:51.004251'),(8,'auth','0002_alter_permission_name_max_length','2020-12-21 06:19:52.958802'),(9,'auth','0003_alter_user_email_max_length','2020-12-21 06:19:53.514291'),(10,'auth','0004_alter_user_username_opts','2020-12-21 06:19:53.639194'),(11,'auth','0005_alter_user_last_login_null','2020-12-21 06:19:55.228089'),(12,'auth','0006_require_contenttypes_0002','2020-12-21 06:19:55.410749'),(13,'auth','0007_alter_validators_add_error_messages','2020-12-21 06:19:55.552353'),(14,'auth','0008_alter_user_username_max_length','2020-12-21 06:19:57.628362'),(15,'auth','0009_alter_user_last_name_max_length','2020-12-21 06:19:59.585342'),(16,'auth','0010_alter_group_name_max_length','2020-12-21 06:20:00.003032'),(17,'auth','0011_update_proxy_permissions','2020-12-21 06:20:00.125121'),(18,'sessions','0001_initial','2020-12-21 06:20:00.820526'),(19,'EmployeeApp','0002_projects_tasks','2020-12-30 09:17:29.101477');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0aari5o7b4fyoar8zbgalh9v1k662etf','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-05 17:12:44.379049'),('0ejhvqaf8rrogmwzogxmugqy5b2kw1fu','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-05 16:49:42.111834'),('2od4j9y3kwmtbbe7ln1p16dh4wyi239q','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-05 07:42:38.807555'),('4pf8rjosduhwxfm5lfa3gugzmt1briow','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-05 16:50:44.486579'),('8k1aycpgtl0g14shku4c7fm4m6s5s50s','NGEzYWZmNmU2NWJlNmQ4N2FkMjI3NjQ4ZDgxNGQ3OWJjMzVhZjkzOTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MDBkMzc0NzA0Y2JiYjJlYTVjNzRjODcxZDk5OGEyNGNiMmU4MTE3In0=','2021-01-04 06:20:55.736488'),('96xes5hayw55rosx8yo1wxxnabggltjx','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 07:20:26.643397'),('im2grim26bk22ia2n09ekqay1uwkjydk','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 07:21:29.402611'),('qisevdsdwltd2ju6zcli7v5gv60327gb','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 11:14:56.115099'),('r4acbg39zex0bxm6gmbkdc8rip7bip8e','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 05:40:02.204363'),('tgdr6ds7wq55723avbd7q6m71896w2nz','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-05 16:41:42.068124'),('tiw4rm6qs7t0bo38wmxqij81lblfhc4e','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 11:15:08.360545'),('wbf1f58428fw6rh16m5osqz4glhqt26q','M2Q2MzBkNmRkMjAzNjhmYzdlNjkwNGFmZTQ0OWNhOWZjYmVjOWRjMDp7ImxvZ2dlZGluX3VzZXJfdXNlcm5hbWUiOiJhZG1pbiIsImxvZ2dlZGluX3VzZXJfbmFtZSI6ImFkbWluIGVtcGxveWVlIn0=','2021-01-06 07:32:14.160012');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_Departments`
--

DROP TABLE IF EXISTS `emp_Departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_Departments` (
  `DepartmentId` int NOT NULL AUTO_INCREMENT,
  `DepartmentName` varchar(100) NOT NULL,
  PRIMARY KEY (`DepartmentId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_Departments`
--

LOCK TABLES `emp_Departments` WRITE;
/*!40000 ALTER TABLE `emp_Departments` DISABLE KEYS */;
INSERT INTO `emp_Departments` VALUES (2,'Admin'),(3,'ECE'),(4,'MECH'),(5,'FRONTEND'),(6,'Backend'),(7,'TESTING'),(8,'NETWORKING');
/*!40000 ALTER TABLE `emp_Departments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_employees`
--

DROP TABLE IF EXISTS `emp_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_employees` (
  `EmployeeId` int NOT NULL AUTO_INCREMENT,
  `EmployeeName` varchar(100) NOT NULL,
  `Department` varchar(100) NOT NULL,
  `DateOfJoining` date NOT NULL,
  `PhotoFileName` varchar(100) NOT NULL,
  PRIMARY KEY (`EmployeeId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_employees`
--

LOCK TABLES `emp_employees` WRITE;
/*!40000 ALTER TABLE `emp_employees` DISABLE KEYS */;
INSERT INTO `emp_employees` VALUES (1,'vijihgy','Backend','2020-12-21','BeautyPlus_20190710111240328_save_bOtSX0Q.jpg'),(2,'ramya','MECH','2020-12-21','ramya.jpg'),(3,'udhaya','TESTING','2020-12-24','anonymous.png'),(4,'lashmi','NETWORKING','2020-12-30','BeautyPlus_20190710111240328_save.jpg');
/*!40000 ALTER TABLE `emp_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_projects`
--

DROP TABLE IF EXISTS `emp_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_projects` (
  `projectId` int NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `project_url` varchar(100) NOT NULL,
  PRIMARY KEY (`projectId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_projects`
--

LOCK TABLES `emp_projects` WRITE;
/*!40000 ALTER TABLE `emp_projects` DISABLE KEYS */;
INSERT INTO `emp_projects` VALUES (1,'lucid','www.lucid.com'),(2,'ptdr','ptdr.com');
/*!40000 ALTER TABLE `emp_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_tasks`
--

DROP TABLE IF EXISTS `emp_tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_tasks` (
  `taskId` int NOT NULL AUTO_INCREMENT,
  `task_project` varchar(100) NOT NULL,
  `task_emp` varchar(100) NOT NULL,
  `task_desc` varchar(100) NOT NULL,
  `task_work_hrs` smallint DEFAULT NULL,
  PRIMARY KEY (`taskId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_tasks`
--

LOCK TABLES `emp_tasks` WRITE;
/*!40000 ALTER TABLE `emp_tasks` DISABLE KEYS */;
INSERT INTO `emp_tasks` VALUES (1,'Lucid','ramya','Need to complete employee management',1);
/*!40000 ALTER TABLE `emp_tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emp_users`
--

DROP TABLE IF EXISTS `emp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emp_users` (
  `UserId` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emp_users`
--

LOCK TABLES `emp_users` WRITE;
/*!40000 ALTER TABLE `emp_users` DISABLE KEYS */;
INSERT INTO `emp_users` VALUES (1,'viji','k','vijik','123'),(2,'sathish','k','sathishk','123'),(3,'admin','employee','admin','123');
/*!40000 ALTER TABLE `emp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-31 11:55:46
