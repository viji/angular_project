from django.db import models

# Create your models here.

class Departments(models.Model):
    DepartmentId = models.AutoField(primary_key=True)
    DepartmentName = models.CharField(max_length=100)
    class Meta:
        db_table="emp_Departments"

class Employees(models.Model):
    EmployeeId = models.AutoField(primary_key=True)
    EmployeeName = models.CharField(max_length=100)
    Department = models.CharField(max_length=100)
    DateOfJoining = models.DateField()
    PhotoFileName = models.CharField(max_length=100)
    class Meta:
        db_table="emp_employees"

class Users(models.Model):
    UserId = models.AutoField(primary_key=True)
    firstName = models.CharField(max_length=100)
    lastName = models.CharField(max_length=100)
    username= models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    class Meta:
        db_table="emp_users"

class Projects(models.Model):
    projectId = models.AutoField(primary_key=True)
    project_name = models.CharField(max_length=100)
    project_url= models.CharField(max_length=100)
    class Meta:
        db_table="emp_projects"

class Tasks(models.Model):
    taskId = models.AutoField(primary_key=True)
    task_project = models.CharField(max_length=100)
    task_emp= models.CharField(max_length=100)
    task_desc= models.CharField(max_length=100)
    task_work_hrs = models.SmallIntegerField(null=True)
    class Meta:
        db_table="emp_tasks"